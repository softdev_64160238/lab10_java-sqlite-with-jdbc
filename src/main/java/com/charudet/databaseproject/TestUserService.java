/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.charudet.databaseproject;

import com.charudet.databaseproject.model.User;
import com.charudet.databaseproject.service.UserService;

/**
 *
 * @author User
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userservicee = new UserService();
        User user = userservicee.login("charudet", "password");
        if(user != null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("!!!Error!!!");
        }
    }
}
